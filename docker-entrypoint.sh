#!/usr/bin/env bash

set -e
WORKSPACE=/opt/sbom-repo-service
cd ${WORKSPACE}

git pull

/bin/bash gradlew bootWar

java -XX:+HeapDumpOnOutOfMemoryError -Xmx14g -Xms512m -jar ${WORKSPACE}/build/libs/sbom-repo-service-1.0-SNAPSHOT.war --spring.profiles.active=prod