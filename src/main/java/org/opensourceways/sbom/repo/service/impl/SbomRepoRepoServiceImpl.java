package org.opensourceways.sbom.repo.service.impl;

import com.alibaba.fastjson2.JSON;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.opensourceways.sbom.repo.constant.SbomRepoConstants;
import org.opensourceways.sbom.repo.model.vo.*;
import org.opensourceways.sbom.repo.service.SbomRepoService;
import org.opensourceways.sbom.repo.utils.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;


@Service
public class SbomRepoRepoServiceImpl implements SbomRepoService {

    private static final Logger logger = LoggerFactory.getLogger(SbomRepoRepoServiceImpl.class);

    @Value("${openeuler.sbom.tools}")
    private String openeulerSbomToolsPath;

    @Value("openeuler.sbom.url")
    private String openeulerDownloadUrl;

    @Override
    public OpenEulerSbomResponse generateOpenEulerSbom(OpenEulerSbomRequest request) {
        OpenEulerSbomResponse response = new OpenEulerSbomResponse();
        File tmpFile = null;
        FileReader sbomReader = null;

        try {
            if (!StringUtils.hasText(request.getArtifactPath())) {
                throw new RuntimeException("artifactPath in request is empty");
            }
            if (!FileUtils.getFile(request.getArtifactPath()).exists()) {
                throw new RuntimeException("artifact file is not exists");
            }

            Path tmpFilePath = Files.createTempFile(SbomRepoConstants.TMP_FILE_PREFIX, SbomRepoConstants.TMP_FILE_SUFFIX);
            tmpFile = tmpFilePath.toFile();
            tmpFile.deleteOnExit();

            int returnValue = runTools(request, tmpFilePath);
            if (returnValue != 0) {
                logger.error("process internal error!");
                response.setErrorInfo("process internal error!");
                response.setSuccess(false);
                return response;
            }

            sbomReader = new FileReader(tmpFile);
            String sbomStr = IOUtils.toString(sbomReader);
            logger.info("sbomStr length: {}", sbomStr.length());

            response.setSuccess(true);
            response.setSbomContent(sbomStr);

            LocalFileWriter.writeToFileNIO(sbomStr);

            logger.info("sbom tools command finish");

        } catch (IOException | InterruptedException | RuntimeException e) {
            logger.error("", e);
            response.setErrorInfo(e.getMessage());
            response.setSuccess(false);
            return response;
        } finally {
            IOUtils.closeQuietly(sbomReader);
            if (tmpFile != null) {
                boolean deleteResult = tmpFile.delete();
                logger.info("tmp file delete result: {}", deleteResult);
            }
        }

        return response;
    }

    @Override
    public OpenEulerLicenseResponse complianceVerification(String url, String path) throws Exception {

        List<String> packageList = new CopyOnWriteArrayList<>(UrlUtils.outPutFileList(url + "Packages/"));
        List<String> repodataList = new CopyOnWriteArrayList<>(UrlUtils.outPutFileList(url + "repodata/"));

//        String pathPackage = "D:\\openEuler-tmp" + path + "\\Packages";
//        String pathRepoData = "D:\\openEuler-tmp" + path + "\\repodata";

        String pathPackage = "/opt/openEuler-tmp"+ path +"/Packages";
        String pathRepoData = "/opt/openEuler-tmp" + path +"/repodata";

        for (String pack : packageList) {
            String downloadUrl = url + "Packages/" + pack;
            DownLoadUtils.downLoadFileFromUrl(downloadUrl, pathPackage);
        }
        for (String repodata : repodataList) {
            String downloadUrl = url + "repodata/" + repodata;
            DownLoadUtils.downLoadFileFromUrl(downloadUrl, pathRepoData);
        }

        //sbom解析
        OpenEulerSbomRequest openEulerSbomRequest = new OpenEulerSbomRequest();
//        openEulerSbomRequest.setArtifactPath("D:\\openEuler-tmp" + path);
        openEulerSbomRequest.setArtifactPath("/opt/openEuler-tmp" + path);
        //license合规判断返回结果
        String json = generateOpenEulerSbom(openEulerSbomRequest).getSbomContent();
        List<PackageLicense> packageLicenseList = new ArrayList<>();
        JSONObject jsonObject = JSON.parseObject(json);
        JSONArray jsonArray = JSON.parseArray(jsonObject.getString("packages"));
        HttpUtil httpUtil = new HttpUtil();
        //获取信息 包名 SPDXID  license信息 licenseDeclared
        for (int i = 0; i < jsonArray.size(); i++) {
            PackageLicense packageLicense = new PackageLicense();
            JSONObject spdxObject = jsonArray.getJSONObject(i);
            String isCom;
            try {
                isCom = httpUtil.get(spdxObject.getString("licenseDeclared"));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            packageLicense.setSpdxId(spdxObject.getString("name"));
            packageLicense.setLicense(spdxObject.getString("licenseDeclared"));
            packageLicense.setResult(isCom.replace("\"", ""));
            packageLicenseList.add(packageLicense);
        }
        OpenEulerLicenseResponse openEulerLicenseResponse = new OpenEulerLicenseResponse();
        openEulerLicenseResponse.setPackageLicenseList(packageLicenseList);
        for (PackageLicense packageLicense : packageLicenseList) {
            if (!packageLicense.getResult().equals("ALLOW")) {
                openEulerLicenseResponse.setResult("FAILED");
                logger.info("license 解析完成" + url);
                return openEulerLicenseResponse;
            }
        }
        openEulerLicenseResponse.setResult("SUCCESS");
//        String directoryPath = "D:\\openEuler-tmp" + path;
        String directoryPath = "/opt/openEuler-tmp" + path;
        File directory = new File(directoryPath);
        DeleteDirectory.deleteDirectory(directory);
        logger.info("license解析完成" + url);
        return openEulerLicenseResponse;
    }

    @Override
    public void generateSbomFile() {
        String localAddress = "/opt/sbom";
        try {
            //下载最新的转测版本
            DownLoadUtils.downLoadFileFromUrl(openeulerDownloadUrl,localAddress);
        }catch (Exception e) {
            logger.info("下载异常，请检查下载源");
        }

        OpenEulerSbomRequest request = new OpenEulerSbomRequest();
        request.setArtifactPath(localAddress);
        //生成sbom文件
        String sbomJson = generateOpenEulerSbom(request).getSbomContent();

        JSONObject sbomJsonObject = JSONObject.parseObject(sbomJson);
        sbomJsonObject.getJSONObject("creationInfo").remove("licenseListVersion");
        JSONArray jsonArray = new JSONArray(sbomJsonObject.get("packages"));
        JSONArray jsonArrayr = new JSONArray(sbomJsonObject.get("relationships"));
        JSONArray jsonArrayPackage = (JSONArray) jsonArray.get(0);
        JSONArray jsonArrayRelationship = (JSONArray) jsonArrayr.get(0);
        for (int i = 0; i < jsonArrayPackage.size(); i++) {
            JSONObject jsonObject = jsonArrayPackage.getJSONObject(i);
            jsonObject.remove("licenseConcluded");
            jsonObject.remove("licenseDeclared");
            UUID uuid = UUID.randomUUID();
            String idString = uuid.toString();
            for (Object relation : jsonArrayRelationship) {
                com.alibaba.fastjson2.JSONObject jsonObject1 = (com.alibaba.fastjson2.JSONObject) relation;
                if (jsonObject.get("SPDXID").equals(jsonObject1.get("spdxElementId"))) {
                    jsonObject1.put("spdxElementId", jsonObject1.get("spdxElementId") + "-" + idString);
                }
                if (jsonObject.get("SPDXID").equals(jsonObject1.get("relatedSpdxElement"))) {
                    jsonObject1.put("relatedSpdxElement", jsonObject1.get("relatedSpdxElement") + "-" + idString);
                }
            }
            jsonObject.put("SPDXID",jsonObject.get("SPDXID") + "-" + idString);
        }
        sbomJsonObject.getJSONObject("creationInfo").put("creators","[openeuler_creator]");
        String versionFiled = sbomJsonObject.get("name").toString();
        String httpUrl = sbomJsonObject.get("documentNamespace").toString();
        int firstIndex = httpUrl.indexOf(versionFiled);
        String code = httpUrl.substring(firstIndex + versionFiled.length(), httpUrl.length() - 1);
        sbomJsonObject.put("documentNamespace","https://repo.openeuler.org/openEuler-24.03-LTS/aarch64/" + code);
        String rectificationResult = sbomJsonObject.toString();
        LocalFileWriter.writeSbomFile(rectificationResult);
    }


    private int runTools(OpenEulerSbomRequest request, Path tmpFilePath) throws IOException, InterruptedException {
        String sbomJsonPath = tmpFilePath.toAbsolutePath().toString();

        List<String> commandList = List.of(openeulerSbomToolsPath,
                request.getArtifactPath(),
                "-o",
                "spdx-json=" + sbomJsonPath);
        logger.info("sbom tools command:{}", commandList);

        ProcessBuilder pb = new ProcessBuilder(commandList);
        Process process = pb.start();

        LogUtils.printMessage(process.getInputStream());
        LogUtils.printMessage(process.getErrorStream());
        return process.waitFor();
    }


}
