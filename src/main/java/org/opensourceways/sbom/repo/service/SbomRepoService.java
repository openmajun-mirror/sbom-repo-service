package org.opensourceways.sbom.repo.service;

import org.opensourceways.sbom.repo.model.vo.*;
import org.springframework.scheduling.annotation.Async;


public interface SbomRepoService {

    OpenEulerSbomResponse generateOpenEulerSbom(OpenEulerSbomRequest request);

    OpenEulerLicenseResponse complianceVerification(String url, String path) throws Exception;

    @Async
    void generateSbomFile();
}
