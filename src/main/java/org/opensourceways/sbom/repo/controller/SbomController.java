package org.opensourceways.sbom.repo.controller;


import org.opensourceways.sbom.repo.model.vo.OpenEulerLicenseResponse;
import org.opensourceways.sbom.repo.model.vo.OpenEulerSbomRequest;
import org.opensourceways.sbom.repo.model.vo.OpenEulerSbomResponse;
import org.opensourceways.sbom.repo.service.SbomRepoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;



@Controller
@RequestMapping(path = "/sbom-repo-api")
public class SbomController {

    private static final Logger logger = LoggerFactory.getLogger(SbomController.class);

    private static final AtomicInteger threadCount = new AtomicInteger(0);

    @Autowired
    private SbomRepoService sbomRepoService;

    private final ExecutorService executorService = Executors.newFixedThreadPool(5);


    @PostMapping("/generateOpenEulerSbom")
    public @ResponseBody ResponseEntity generateOpenEulerSbom(HttpServletRequest request, @RequestBody OpenEulerSbomRequest sbomRequest) {
        logger.info("generate openEuler sbom, artifact path: {}", sbomRequest.getArtifactPath());

        OpenEulerSbomResponse sbomResponse = sbomRepoService.generateOpenEulerSbom(sbomRequest);
        return ResponseEntity.status(HttpStatus.OK).body(sbomResponse);
    }

    @PostMapping("/licenseCheck")
    public @ResponseBody ResponseEntity cmplianceVerification(@RequestParam String url) {
        OpenEulerLicenseResponse response = new OpenEulerLicenseResponse();
        response.setResult("SUCCESS");
        response.setPackageLicenseList(Collections.emptyList());
        return ResponseEntity.status(HttpStatus.OK).body(response);
        // Future<OpenEulerLicenseResponse> future;
        // try {
        // future = executorService.submit(() -> {
        // // 调用业务逻辑
        // logger.info("License check start, url : " + url);
        // String threadId = String.valueOf(threadCount.incrementAndGet());
        // return sbomRepoService.complianceVerification(url, threadId);
        // });
        // } catch (RejectedExecutionException e) {
        // logger.error("线程池已满，无法接受新的任务", e);
        // OpenEulerLicenseResponse openEulerLicenseResponse = new
        // OpenEulerLicenseResponse();
        // openEulerLicenseResponse.setResult("线程资源已满，请稍后重试");
        // return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE)
        // .body(openEulerLicenseResponse);
        // }

        // try {
        // // 返回任务结果
        // return ResponseEntity.status(HttpStatus.OK).body(future.get());
        // } catch (ExecutionException | InterruptedException e) {
        // logger.error("线程异常:" + e.getCause().getMessage());
        // return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal
        // server error");
        // }
    }

    @PostMapping("/generateSbomFile")
    public ResponseEntity generateSbomFile() {
        sbomRepoService.generateSbomFile();
        return ResponseEntity.status(HttpStatus.OK).body("sbom文件生成中");
    }
}
