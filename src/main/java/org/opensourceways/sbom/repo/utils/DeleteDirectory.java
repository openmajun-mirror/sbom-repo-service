package org.opensourceways.sbom.repo.utils;

import java.io.File;

public class DeleteDirectory {
    public static boolean deleteDirectory(File directory) {
        if (!directory.exists()) {
            System.out.println("Directory does not exist.");
            return false;
        }

        // 如果是目录，则递归删除子文件和子目录
        if (directory.isDirectory()) {
            File[] files = directory.listFiles();
            if (files != null) {
                for (File file : files) {
                    deleteDirectory(file);
                }
            }
        }

        // 删除文件或空目录
        return directory.delete();
    }
}
