package org.opensourceways.sbom.repo.utils;

import okhttp3.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class HttpUtil {
    public String get(String param) throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .build();
        MediaType mediaType = MediaType.parse("text/plain");
        RequestBody body = new FormBody.Builder()
                .add("license",param)
                .build();
        Request request = new Request.Builder()
                .url("https://compliance.openeuler.org/check")
                .post(body)
                .addHeader("Accept", "*/*")
                .addHeader("Host", "compliance.openeuler.org")
                .addHeader("Connection", "keep-alive")
                .build();
        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }catch (java.net.SocketTimeoutException e) {
            return "请求超时";
        }
    }
}
