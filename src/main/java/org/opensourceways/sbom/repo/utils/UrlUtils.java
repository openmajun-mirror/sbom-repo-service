package org.opensourceways.sbom.repo.utils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UrlUtils {

    private static final Logger logger = LoggerFactory.getLogger(UrlUtils.class);
    /**
     * 获取url文件列表
     * @param url
     * @return
     * @throws IOException
     */
    public static List<String> outPutFileList (String url) {
        List<String> elementNameList = new ArrayList<>();
        try {
            Document document = Jsoup.connect(url).get();
            Elements links = document.select("a");
            for(Element link : links) {
                String element = link.attr("href");
                elementNameList.add(element);
            }
            elementNameList.remove(0);
        } catch (IOException e) {
           logger.info("url 链接超时");
        }
        return elementNameList;
    }
}
