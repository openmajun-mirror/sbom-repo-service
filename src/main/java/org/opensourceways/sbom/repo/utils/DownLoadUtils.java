package org.opensourceways.sbom.repo.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

import java.util.List;
import java.util.Map;

@Component
public class DownLoadUtils {

    private static final Logger logger = LoggerFactory.getLogger(DownLoadUtils.class);

    public static void downLoadFileFromUrl(String urlStr, String savePath) throws Exception {
        int attempt = 0;
        boolean success = false;

        while (attempt < 3 && !success) {
            attempt++;
            URL url = new URL(urlStr);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            // 设置超时时间为3秒
            conn.setConnectTimeout(10 * 1000);
            // 防止屏蔽程序抓取而返回403错误
            conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");
            // 获取重定向地址
            String newUrl = getRedirectedUrl(conn);
            if (!newUrl.equals(urlStr)) {
                url = new URL(newUrl);
                conn = (HttpURLConnection) url.openConnection();
            }

            // 下载文件
            try (InputStream inputStream = conn.getInputStream();
                 FileOutputStream fos = new FileOutputStream(getFilePath(urlStr, savePath))) {

                byte[] getData = readInputStream(inputStream);
                fos.write(getData);
            } catch (IOException e) {
                logger.error("Error downloading file from URL: " + url, e);
                throw e;
            }
            logger.info(url + " download success");
        }
    }

    private static String getRedirectedUrl(HttpURLConnection conn) {
        Map<String, List<String>> map = conn.getHeaderFields();
        for (String key : map.keySet()) {
            if ("Location".equalsIgnoreCase(key)) {
                return map.get(key).get(0);
            }
        }
        return conn.getURL().toString();
    }

    private static String getFilePath(String urlStr, String savePath) {
        File saveDir = new File(savePath);
        if (!saveDir.exists()) {
            saveDir.mkdirs();
        }
        String fileName = urlStr.substring(urlStr.lastIndexOf("/") + 1);
        return saveDir + File.separator + fileName;
    }

    /**
     * 从输入流中获取字节数组
     * @param inputStream
     * @return byte[]
     * @throws IOException
     */
    public static byte[] readInputStream(InputStream inputStream) throws IOException {
        byte[] buffer = new byte[8192]; // 增加缓冲区大小
        int len;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        while ((len = inputStream.read(buffer)) != -1) {
            bos.write(buffer, 0, len);
        }
        return bos.toByteArray();
    }

}
