package org.opensourceways.sbom.repo.utils;

import org.opensourceways.sbom.repo.service.impl.SbomRepoRepoServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class LocalFileWriter {

    private static final Logger logger = LoggerFactory.getLogger(LocalFileWriter.class);

    public static void writeSbomFile(String content) {
        try {
            Files.write(Paths.get("/opt/sbom/sbomfiles"), content.getBytes(StandardCharsets.UTF_8));
            logger.info("sbom文件已生成");
        } catch (IOException e) {
            logger.error("写入文件错误");
        }
    }

    public static void writeToFileNIO(String content) {
        String data = content;
        try {
            Files.write(Paths.get("D:\\sbom\\sbom-file\\Sbom-json\\openEuler-24.09-LTS-EPOL-x86_64.spdx.json"), data.getBytes(StandardCharsets.UTF_8));
            System.out.println("数据已成功写入文件。");
        } catch (IOException e) {
            System.out.println("发生错误。");
        }
    }
}
