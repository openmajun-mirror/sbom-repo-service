package org.opensourceways.sbom.repo;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class RequestLimitInterceptor extends HandlerInterceptorAdapter {

  private static final int MAX_REQUESTS = 5;
  private static int requestCount = 0;

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    if (requestCount < MAX_REQUESTS) {
      requestCount++;
      return true;
    } else {
      response.setStatus(HttpServletResponse.SC_CONFLICT);
      return false;
    }
  }

  @Override
  public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
    requestCount--;
  }
}
