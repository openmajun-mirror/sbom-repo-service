package org.opensourceways.sbom.repo.model.vo;

import java.util.List;

public class OpenEulerLicenseResponse {

    private String result;

    private List<PackageLicense> packageLicenseList;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public List<PackageLicense> getPackageLicenseList() {
        return packageLicenseList;
    }

    public void setPackageLicenseList(List<PackageLicense> packageLicenseList) {
        this.packageLicenseList = packageLicenseList;
    }
}
