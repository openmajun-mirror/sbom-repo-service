package org.opensourceways.sbom.repo.model.vo;

import java.io.Serializable;

public class OpenEulerDownloadRequest implements Serializable {

    private String openEulerBranch;

    private String openEulerImageFormat;

    private String openEulerArch;

    private String fileUrl;

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getOpenEulerArch() {
        return openEulerArch;
    }

    public void setOpenEulerArch(String openEulerArch) {
        this.openEulerArch = openEulerArch;
    }

    public String getOpenEulerImageFormat() {
        return openEulerImageFormat;
    }

    public void setOpenEulerImageFormat(String openEulerImageFormat) {
        this.openEulerImageFormat = openEulerImageFormat;
    }

    public String getOpenEulerBranch() {
        return openEulerBranch;
    }

    public void setOpenEulerBranch(String openEulerBranch) {
        this.openEulerBranch = openEulerBranch;
    }

}
