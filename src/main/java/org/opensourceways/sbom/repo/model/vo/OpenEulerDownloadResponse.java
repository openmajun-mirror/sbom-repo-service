package org.opensourceways.sbom.repo.model.vo;

public class OpenEulerDownloadResponse {

    private String MessageInfo;

    public String getMessageInfo() {
        return MessageInfo;
    }

    public void setMessageInfo(String messageInfo) {
        MessageInfo = messageInfo;
    }
}
